﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class MatrixFilter2dScharrFilter : MatrixFilter2d
    {
        public MatrixFilter2dScharrFilter()
        {
            kernel1 = new float[3, 3]{{3, 0, -3}, 
                                      {10, 0, -10}, 
                                      {3, 0, -3}};

            kernel2 = new float[3, 3]{{3, 10, 3}, 
                                      {0, 0, 0}, 
                                      {-3, -10, -3}};
        }
    }
}
