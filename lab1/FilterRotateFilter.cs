﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterRotateFilter : Filters
    {
        protected int x0 = 0;
        protected int y0 = 0;
        protected int m0 = 45;

        public FilterRotateFilter() { }

        public FilterRotateFilter(int x0, int y0, int m0)
        {
            this.x0 = x0;
            this.y0 = y0;
            this.m0 = m0;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            double m = m0 / 180.0 * (float)Math.PI;
            int a = (int)((x - x0) * Math.Cos(m) - (y - y0) * Math.Sin(m) + x0);
            int b = (int)((x - x0) * Math.Sin(m) + (y - y0) * Math.Cos(m) + y0);
            if (a > sourceImage.Width - 1 || a < 0 || b > sourceImage.Height - 1 || b < 0)
                return Color.FromArgb(0, 0, 0);
            else
                return Color.FromArgb(sourceImage.GetPixel(a, b).R, sourceImage.GetPixel(a, b).G, sourceImage.GetPixel(a, b).B);
        }
    }
}
