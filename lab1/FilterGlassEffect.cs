﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterGlassEffect : Filters
    {
        protected static int value = 10;
        protected int half = value / 2;
        Random r = new Random(value);
        
        public FilterGlassEffect() { }

        public FilterGlassEffect(int koef)
        {
            value = koef;
            half = value / 2;
            r = new Random(value);
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int randx = r.Next(value);
            int randy = r.Next(value);
            return Color.FromArgb(sourceImage.GetPixel(Clamp((int)(x + (randx - half)), 0, sourceImage.Width - 1), Clamp((int)(y + (randy - half)), 0, sourceImage.Height - 1)).R,
                                  sourceImage.GetPixel(Clamp((int)(x + (randx - half)), 0, sourceImage.Width - 1), Clamp((int)(y + (randy - half)), 0, sourceImage.Height - 1)).G,
                                  sourceImage.GetPixel(Clamp((int)(x + (randx - half)), 0, sourceImage.Width - 1), Clamp((int)(y + (randy - half)), 0, sourceImage.Height - 1)).B
                                  );
        }
    }
}
