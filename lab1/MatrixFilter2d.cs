﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    class MatrixFilter2d : MatrixFilter
    {
        protected float[,] kernel1 = null;
        protected float[,] kernel2 = null;
        protected bool flag = true;

        protected MatrixFilter2d() { }

        public MatrixFilter2d(float[,] kernel1, float[,] kernel2)
        {
            this.kernel1 = kernel1;
            this.kernel2 = kernel2;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            kernel = kernel1;
            Color color1 = base.calculateNewPixelColor(sourceImage, x, y);
            kernel = kernel2;
            Color color2 = base.calculateNewPixelColor(sourceImage, x, y);

            int resultR = (int)Math.Sqrt((color1.R * color1.R) + (color2.R * color2.R));
            int resultG = (int)Math.Sqrt((color1.G * color1.G) + (color2.G * color2.G));
            int resultB = (int)Math.Sqrt((color1.B * color1.B) + (color2.B * color2.B));
            
            return Color.FromArgb(Clamp((int)resultR, 0, 255),
                                  Clamp((int)resultG, 0, 255),
                                  Clamp((int)resultB, 0, 255)
                                  );
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            Bitmap image = Clone(sourceImage);
            
            if (flag)
            {
                num = num + num;
                Filters filter = new FilterGrayScale(num);
                image = filter.processImage(sourceImage, worker);
            }
            return base.processImage(image, worker);
        }
    }
}
