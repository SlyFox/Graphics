﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class MatrixFilter2dSobelFilter : MatrixFilter2d
    {
        public MatrixFilter2dSobelFilter()
        {
            kernel1 = new float[3, 3]{{-1, 0, 1}, 
                                      {-2, 0, 2}, 
                                      {-1, 0, 1}};

            kernel2 = new float[3, 3]{{-1, -2, -1}, 
                                      {0, 0, 0}, 
                                      {1, 2, 1}};
        }

        public MatrixFilter2dSobelFilter(int n) : this()
        {
            num = n;
            this.flag = false;
        }
    }
}
