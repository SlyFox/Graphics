﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    class GrayWorld : Filters
    {
        protected long sumR = 0;
        protected long sumG = 0;
        protected long sumB = 0;

        protected bool findSums(Bitmap sourceImage, BackgroundWorker worker)
        {
            if (sourceImage != null)
            {
                int a = 100 / num;
                for (int i = 0; i < sourceImage.Width; i++)
                {
                    worker.ReportProgress((int)((float)i / sourceImage.Width * a + b));
                    if (worker.CancellationPending)
                        return true;
                    for (int j = 0; j < sourceImage.Height; j++)
                    {
                        Color sourceColor = sourceImage.GetPixel(i, j);
                        sumR += sourceColor.R;
                        sumG += sourceColor.G;
                        sumB += sourceColor.B;
                    }
                }
                b += a;
                return false;
            }
            return true;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y){
            Color sourceColor = sourceImage.GetPixel(x, y);
            int ansR = Clamp((int)((sumR + sumG + sumB) * sourceColor.R / (3 * sumR)), 0, 255);
            int ansG = Clamp((int)((sumR + sumG + sumB) * sourceColor.G / (3 * sumG)), 0, 255);
            int ansB = Clamp((int)((sumR + sumG + sumB) * sourceColor.B / (3 * sumB)), 0, 255);
            return Color.FromArgb(ansR, ansG, ansB);
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker){
            sumR = 0;
            sumG = 0;
            sumB = 0;
            if (findSums(sourceImage, worker))
                return null;
            return base.processImage(sourceImage, worker);
        }

        public GrayWorld()
        {
            num = 2;
        }
    }
}
