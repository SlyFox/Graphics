﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterWaves1 : Filters
    {
        protected int koef = 60;

        public FilterWaves1() { }

        public FilterWaves1(int k)
        {
            koef = k;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int k = (int)(x + 20 * Math.Sin(2 * Math.PI * y / koef));
            return Color.FromArgb(sourceImage.GetPixel(Clamp(k, 0, sourceImage.Width - 1), y).R,
                                  sourceImage.GetPixel(Clamp(k, 0, sourceImage.Width - 1), y).G,
                                  sourceImage.GetPixel(Clamp(k, 0, sourceImage.Width - 1), y).B
                                  );
        }
    }
}
