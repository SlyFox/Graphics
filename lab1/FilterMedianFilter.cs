﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterMedianFilter : Filters
    {
        protected int k = 1;

        public FilterMedianFilter() { }

        public FilterMedianFilter(int k)
        {
            this.k = k;
        }

        public FilterMedianFilter(int k, int n) : this(k)
        {
            num = n;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int i = 0;
            int n = k + k + 1;
            n *= n;
            int[] masR = new int[n];
            int[] masG = new int[n];
            int[] masB = new int[n];
            for (int l = -k; l <= k; l++)
                for (int z = -k; z <= k; z++)
                {
                    int idx = Clamp(x + z, 0, sourceImage.Width - 1);
                    int idy = Clamp(y + l, 0, sourceImage.Height - 1);
                    Color neighborColor = sourceImage.GetPixel(idx, idy);
                    masR[i] = neighborColor.R;
                    masB[i] = neighborColor.B;
                    masG[i] = neighborColor.G;
                    ++i;
                }
            Array.Sort(masR);
            Array.Sort(masG);
            Array.Sort(masB);
            return Color.FromArgb(masR[n / 2], masG[n / 2], masB[n / 2]);
        }
    }
}
