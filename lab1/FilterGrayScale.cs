﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterGrayScale : Filters
    {
        public FilterGrayScale() { }

        public FilterGrayScale(int n)
        {
            num = n;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            double intensity = 0.36 * sourceColor.R + 0.53 * sourceColor.G + 0.11 * sourceColor.B;
            return Color.FromArgb(Clamp((int)intensity, 0, 255),
                                  Clamp((int)intensity, 0, 255),
                                  Clamp((int)intensity, 0, 255)
                                  );
        }
    }
}
