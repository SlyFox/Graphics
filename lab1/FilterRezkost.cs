﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class FilterRezkost : MatrixFilter
    {
        public FilterRezkost()
        {
            int sizeX = 3;
            int sizeY = 3;
            kernel = new float[sizeX, sizeY];
            int c = -1;
            for (int i = 0; i < sizeX; i++)
                for (int j = 0; j < sizeY; j++)
                {
                    kernel[i, j] = -1 - c;
                    c = (int)kernel[i, j];
                }
            kernel[1, 1] = 5;
        }
    }
}
