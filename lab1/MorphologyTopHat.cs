﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    class MorphologyTopHat : Morphology
    {
        protected override bool comparison(Color sourceColor, double best)
        {
            throw new NotImplementedException();
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            if (sourceImage != null)
            {
                Filters filter = new MorphologyOpening(kernel, num);
                Bitmap image = filter.processImage(sourceImage, worker);
                int a = 100 / this.num;
                int width = sourceImage.Width;
                int height = sourceImage.Height;
                Bitmap resultImage = new Bitmap(width, height);
                for (int i = 0; i < width; i++)
                {
                    worker.ReportProgress((int)((float)i / width * a + b));
                    if (worker.CancellationPending)
                        return null;
                    for (int j = 0; j < height; j++)
                    {
                        Color image1Color = sourceImage.GetPixel(i, j);
                        Color image2Color = image.GetPixel(i, j);
                        resultImage.SetPixel(i, j, Color.FromArgb(Clamp(image1Color.R - image2Color.R, 0, 255),
                                                                    Clamp(image1Color.G - image2Color.G, 0, 255),
                                                                    Clamp(image1Color.B - image2Color.B, 0, 255)
                                                                    ));
                    }
                }
                return resultImage;
            }
            return null;
        }

        public MorphologyTopHat()
        {
            kernel = createSquare(1);
            num = 2;
        }

        public MorphologyTopHat(int[,] kernel)
        {
            this.kernel = kernel;
            num = 2;
        }

        public MorphologyTopHat(string s, int n)
        {
            this.kernel = chooseKernel(s, n);
            num = 2;
        }
    }
}
