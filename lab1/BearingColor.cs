﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class BearingColor : Filters
    {
        protected int x, y;
        protected Color sourceColor;
        protected Color destinationColor;

        public BearingColor(int x, int y, Color sourceColor, Color destinationColor)
        {
            this.x = x;
            this.y = y;
            this.sourceColor = sourceColor;
            this.destinationColor = destinationColor;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color color = sourceImage.GetPixel(x, y);
            int resultR = sourceColor.R == 0 ? destinationColor.R + color.R * (255 - destinationColor.R) / 255 : 
                                               color.R * destinationColor.R / sourceColor.R;
            int resultG = sourceColor.G == 0 ? destinationColor.G + color.G * (255 - destinationColor.G) / 255 : 
                                               color.G * destinationColor.G / sourceColor.G;
            int resultB = sourceColor.B == 0 ? destinationColor.G + color.B * (255 - destinationColor.B) / 255 : 
                                               color.B * destinationColor.B / sourceColor.B;
            return Color.FromArgb(Clamp(resultR, 0, 255), 
                                  Clamp(resultG, 0, 255), 
                                  Clamp(resultB, 0, 255)
                                  );
        }
    }
}
