﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterBrightness : Filters
    {
        protected int k = 20;

        public FilterBrightness() { }

        public FilterBrightness(int k)
        {
            this.k = k;
        }

        public FilterBrightness(int k, int n)
        {
            this.k = k;
            this.num = n;
        }
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            return Color.FromArgb(Clamp((int)(sourceColor.R + k), 0, 255),
                                  Clamp((int)(sourceColor.G + k), 0, 255),
                                  Clamp((int)(sourceColor.B + k), 0, 255)
                                  );
        }
    }
}
