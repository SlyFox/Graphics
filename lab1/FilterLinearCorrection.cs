﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    class FilterLinearCorrection : Filters
    {
        int bigR = 0;
        int bigG = 0;
        int bigB = 0;
        int smallR = 255;
        int smallG = 255;
        int smallB = 255;

        public FilterLinearCorrection()
        {
            num = 2;
        }

        public FilterLinearCorrection(int n)
        {
            num = n + n;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            int ansR = (sourceColor.R - smallR) * 255 / (bigR - smallR);
            int ansG = (sourceColor.G - smallG) * 255 / (bigG - smallG);
            int ansB = (sourceColor.B - smallB) * 255 / (bigB - smallB);
            return Color.FromArgb(ansR, ansG, ansB);
        }

        private bool findMinMax(Bitmap sourceImage, BackgroundWorker worker)
        {
            if (sourceImage != null)
            {
                bigR = 0;
                bigG = 0;
                bigB = 0;
                smallR = 255;
                smallG = 255;
                smallB = 255;
                int a = 100 / num;
                for (int i = 0; i < sourceImage.Width; i++)
                {
                    worker.ReportProgress((int)((float)i / sourceImage.Width * a + b));
                    if (worker.CancellationPending)
                    {
                        return true;
                    }
                    for (int j = 0; j < sourceImage.Height; j++)
                    {
                        Color sourceColor = sourceImage.GetPixel(i, j);
                        bigR = Math.Max(bigR, sourceColor.R);
                        bigG = Math.Max(bigG, sourceColor.G);
                        bigB = Math.Max(bigB, sourceColor.B);
                        smallR = Math.Min(smallR, sourceColor.R);
                        smallG = Math.Min(smallG, sourceColor.G);
                        smallB = Math.Min(smallB, sourceColor.B);
                    }
                }
                b += a;
                return false;
            }
            return true;
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            if (findMinMax(sourceImage, worker))
                return null;
            return base.processImage(sourceImage, worker);
        }
    }
}
