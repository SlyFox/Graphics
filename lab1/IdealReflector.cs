﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    class IdealReflector : Filters
    {
        protected int bigR;
        protected int bigG;
        protected int bigB;

        protected bool findMax(Bitmap sourceImage, BackgroundWorker worker)
        {
            if (sourceImage != null)
            {
                int a = 100 / num;
                for (int i = 0; i < sourceImage.Width; i++)
                {
                    worker.ReportProgress((int)((float)i / sourceImage.Width * a + b));
                    if (worker.CancellationPending)
                        return true;
                    for (int j = 0; j < sourceImage.Height; j++)
                    {
                        Color sourceColor = sourceImage.GetPixel(i, j);
                        bigR = Math.Max(bigR, sourceColor.R);
                        bigG = Math.Max(bigG, sourceColor.G);
                        bigB = Math.Max(bigB, sourceColor.B);
                    }
                }
                b += a;
                return false;
            }
            return true;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y){
            Color sourceColor = sourceImage.GetPixel(x, y);
            int ansR = (int)((long)(sourceColor.R * 255) / (bigR));
            int ansG = (int)((long)(sourceColor.G * 255) / (bigG));
            int ansB = (int)((long)(sourceColor.B * 255) / (bigB));
            return Color.FromArgb(Clamp(ansR, 0, 255), Clamp(ansG, 0, 255), Clamp(ansB, 0, 255));
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            bigR = 0;
            bigG = 0;
            bigB = 0;
            if(findMax(sourceImage, worker))
                return null;
            return base.processImage(sourceImage, worker);
        }

        public IdealReflector()
        {
            num = 2;
        }
    }
}
