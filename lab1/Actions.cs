﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class Actions
    {
        public Stack<Bitmap> stack1 = new Stack<Bitmap>();
        public Stack<Bitmap> stack2 = new Stack<Bitmap>();
        Bitmap currentimage = null;

        public Bitmap undo()
        {
            if (stack1.Count != 0)
            {
                stack2.Push(currentimage);
                currentimage = stack1.Pop();
                return currentimage;
            }
            else
            {
                if (currentimage != null)
                {
                    stack2.Push(currentimage);
                    currentimage = null;
                }
                return null;
            }
        }

        public Bitmap redo()
        {
            if (stack2.Count != 0)
            {
                stack1.Push(currentimage);
                currentimage = stack2.Pop();
                return currentimage;
            }
            else
            {
                return currentimage;
            }
        }

        public void add(Bitmap image)
        {
            stack2.Clear();
            stack1.Push(currentimage);
            currentimage = image;
        }
    }
}
