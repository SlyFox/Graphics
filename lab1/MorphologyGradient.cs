﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    class MorphologyGradient : Morphology
    {
        protected override bool comparison(Color sourceColor, double best)
        {
            throw new NotImplementedException();
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            if (sourceImage != null)
            {
                Filters filter1 = new MorphologyDilation(kernel, num);
                Bitmap image1 = filter1.processImage(sourceImage, worker);
                Filters filter2 = new MorphologyErosion(kernel, num);
                Bitmap image2 = filter2.processImage(sourceImage, worker);
                int a = 100 / this.num;
                int width = sourceImage.Width;
                int height = sourceImage.Height;
                Bitmap resultImage = new Bitmap(width, height);
                for (int i = 0; i < width; i++)
                {
                    worker.ReportProgress((int)((float)i / width * a + b));
                    if (worker.CancellationPending)
                        return null;
                    for (int j = 0; j < height; j++)
                    {
                        Color image1Color = image1.GetPixel(i, j);
                        Color image2Color = image2.GetPixel(i, j);
                        resultImage.SetPixel(i, j, Color.FromArgb(Clamp(image1Color.R - image2Color.R, 0, 255),
                                                                    Clamp(image1Color.G - image2Color.G, 0, 255),
                                                                    Clamp(image1Color.B - image2Color.B, 0, 255)
                                                                    ));
                    }
                }
                return resultImage;
            }
            return null;
        }

        public MorphologyGradient()
        {
            kernel = createSquare(1);
            num = 3;
        }

        public MorphologyGradient(int[,] kernel)
        {
            this.kernel = kernel;
            num = 3;
        }

        public MorphologyGradient(string s, int n)
        {
            this.kernel = chooseKernel(s, n);
            num = 3;
        }
    }
}
