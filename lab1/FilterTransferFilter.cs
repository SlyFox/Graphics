﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterTransferFilter : Filters
    {
        protected int dx = 50;

        public FilterTransferFilter() { }

        public FilterTransferFilter(int x)
        {
            this.dx = x;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            if (x + dx > sourceImage.Width - 1)
            {
                return Color.FromArgb(0, 0, 0);
            }
            else
                return Color.FromArgb(sourceImage.GetPixel(x + dx, y).R,
                                        sourceImage.GetPixel(x + dx, y).G,
                                        sourceImage.GetPixel(x + dx, y).B
                                        );
        }
    }
}
