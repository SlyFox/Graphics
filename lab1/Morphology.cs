﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    abstract class Morphology : Filters
    {
        protected static int[,] createSquare(int n)
        {
            int size = n + n + 1;
            int[,] matrix = new int[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                    matrix[i, j] = 1;
            return matrix;
        }

        protected static int[,] createRhombus(int n)
        {
            int size = n + n + 1;
            int[,] matrix = new int[size, size];
            for (int i = 0; i <= n; i++)
                for (int j = 0; j <= n; j++)
                    matrix[n - i, j] = matrix[n + i, j] = matrix[n - i, size - 1 - j] = matrix[n + i, size - 1 - j] = j >= i ? 1 : 0;
            return matrix;
        }

        protected static int[,] createRound(int n)
        {
            int size = n + n + 1;
            int[,] matrix = new int[size, size];
            for (int i = 0; i <= n; i++)
                for (int j = 0; j <= n; j++)
                    matrix[n - i, n - j] = matrix[n + i, n - j] = matrix[n - i, n + j] = matrix[n + i, n + j] = 
                        (Math.Sqrt(i * i + j * j) - n < 0.41) ? 1 : 0;
            return matrix;
        }

        protected static int[,] chooseKernel(string s, int n)
        {
            switch (s)
            {
                case "round":
                    return createRound(n);
                case "rhombus":
                    return createRhombus(n);
                default:
                    return createSquare(n);
            }
        }

        protected int[,] kernel = null;
        protected int radiusX;
        protected int radiusY;
        protected double bestValue;
        protected const double kR = 0.36;
        protected const double kG = 0.53;
        protected const double kB = 0.11;

        protected Morphology() { }

        public Morphology(int[,] kernel)
        {
            this.kernel = kernel;
            radiusX = this.kernel.GetLength(0) / 2;
            radiusY = this.kernel.GetLength(1) / 2;
        }

        protected abstract bool comparison(Color sourceColor, double best);

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int bestR = 0;
            int bestG = 0;
            int bestB = 0;
            double best = bestValue;

            for (int i0 = -radiusX; i0 <= radiusX; i0++)
                for (int j0 = -radiusY; j0 <= radiusY; j0++)
                {
                    if (kernel[radiusX + i0, radiusY + j0] > 0)
                    {
                        Color sourceColor = sourceImage.GetPixel(Clamp(x + i0, 0, sourceImage.Width - 1), Clamp(y + j0, 0, sourceImage.Height - 1));

                        if (comparison(sourceColor, best))
                        {
                            bestR = sourceColor.R;
                            bestG = sourceColor.G;
                            bestB = sourceColor.B;
                            best = kR * bestR + kG * bestG + kB * bestB;
                        }
                    }
                }
            return Color.FromArgb(bestR, bestG, bestB);
        }

    }
}
