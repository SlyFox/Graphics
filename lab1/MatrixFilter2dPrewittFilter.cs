﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class MatrixFilter2dPrewittFilter : MatrixFilter2d
    {
        public MatrixFilter2dPrewittFilter()
        {
            kernel1 = new float[3, 3]{{-1, 0, 1}, 
                                      {-1, 0, 1}, 
                                      {-1, 0, 1}};

            kernel2 = new float[3, 3]{{-1, -1, -1}, 
                                      {0, 0, 0}, 
                                      {1, 1, 1}};
        }
    }
}
