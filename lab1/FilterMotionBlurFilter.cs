﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    class FilterMotionBlurFilter : MatrixFilter
    {
        protected void createMotionKernel(int n)
        {
            int sizeX = 2 * n + 1;
            int sizeY = 2 * n + 1;
            kernel = new float[sizeX, sizeY];
            for (int i = 0; i < sizeX; i++)
                for (int j = 0; j < sizeY; j++)
                    if (i == j)
                        kernel[i, j] = 1.0f / (float)(sizeX);
                    else
                        kernel[i, j] = 0;
        }

        public FilterMotionBlurFilter(int k = 8)
        {
            createMotionKernel(k);
        }

    }
}
