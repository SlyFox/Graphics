﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class MorphologyErosion : Morphology
    {
        public MorphologyErosion() : base(createSquare(1))
        {
            bestValue = 255;
        }

        public MorphologyErosion(int[,] kernel, int num = 1) : base(kernel)
        {
            bestValue = 255;
            this.num = num;
        }

        public MorphologyErosion(string s, int n, int num = 1) : base(chooseKernel(s, n))
        {
            bestValue = 255;
            this.num = num;
        }

        protected override bool comparison(Color sourceColor, double best)
        {
            return kR * sourceColor.R + kG * sourceColor.G + kB * sourceColor.B <= best;
        }
    }
}
