﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    class FilterGlowingEdges : Filters
    {
        public FilterGlowingEdges()
        {
            num = 3;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            throw new NotImplementedException();
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            Filters filter1 = new FilterMedianFilter(1, num);
            Bitmap image1 = filter1.processImage(sourceImage, worker);
            Filters filter2 = new MatrixFilter2dSobelFilter(num);
            Bitmap image2 = filter2.processImage(image1, worker);
            Filters filter3 = new FilterMaximum(1, num);
            return filter3.processImage(image2, worker);
        }

    }
}
