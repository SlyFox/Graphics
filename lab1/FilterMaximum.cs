﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterMaximum : Filters
    {
        protected int k = 1;

        public FilterMaximum() { }

        public FilterMaximum(int k)
        {
            this.k = k;
        }

        public FilterMaximum(int k, int n) : this(k)
        {
            num = n;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int bestR = 0;
            int bestG = 0;
            int bestB = 0;
            for (int l = -k; l <= k; l++)
                for (int z = -k; z <= k; z++)
                {
                    int idx = Clamp(x + z, 0, sourceImage.Width - 1);
                    int idy = Clamp(y + l, 0, sourceImage.Height - 1);
                    Color neighborColor = sourceImage.GetPixel(idx, idy);
                    
                    bestR = Math.Max(bestR, neighborColor.R);
                    bestG = Math.Max(bestG, neighborColor.G);
                    bestB = Math.Max(bestB, neighborColor.B);
                }
            return Color.FromArgb(bestR, bestG, bestB);
        }
    }
}
