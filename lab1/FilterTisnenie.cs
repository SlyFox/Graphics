﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Drawing;

namespace lab1
{
    class FilterTisnenie : Filters
    {
        protected int brightnessConst = 128;

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color sourceColor = sourceImage.GetPixel(x, y);
            int resultColor = sourceColor.R + sourceColor.G + sourceColor.B;
            resultColor /= 3;
            return Color.FromArgb(resultColor, 
                                  resultColor, 
                                  resultColor
                                  );
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            float[,] kernel = new float[3, 3]{{0, 1, 0},
                                              {1, 0, -1},
                                              {0, -1, 0}};
            Filters filter = new MatrixFilter(kernel, num);
            Bitmap image1 = filter.processImage(sourceImage, worker);
            Filters filter2 = new FilterBrightness(brightnessConst, num);
            Bitmap image2 = filter2.processImage(image1, worker);
            return base.processImage(image2, worker);
        }

        public FilterTisnenie()
        {
            num = 3;
        }

        public FilterTisnenie(int brightness)
        {
            num = 3;
            this.brightnessConst = brightness;
        }
    }
}
