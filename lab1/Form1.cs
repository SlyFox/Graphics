﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public partial class Form1 : Form
    {
        Bitmap image;
        Actions history = new Actions();
        bool permitClickOnPictureBox1 = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void открыьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files | *.png; *.jpg; *.bmp | All Files (*.*) | *.*";

            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK) {
                image = new Bitmap(dialog.FileName);

                pictureBox1.Image = image;
                history.add(image);
                pictureBox1.Refresh();
            }

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            ((Filters)e.Argument).setBtoZero();
            Bitmap newImage = ((Filters)e.Argument).processImage(image, backgroundWorker1);
            if (backgroundWorker1.CancellationPending != true)
            {
                image = newImage;
                history.add(image);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled)
            {
                pictureBox1.Image = image;
                pictureBox1.Refresh();
            }
            progressBar1.Value = 0;
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Title = "Сохранить изображение как...";
            dialog.OverwritePrompt = true;
            dialog.CheckPathExists = true;
            dialog.Filter = ".JPG | *.jpg |.PNG | *.png | .BMP | *.bmp | All Files (*.*) | *.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
                catch
                {
                    MessageBox.Show("Невозможно сохранить изображение", "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void отменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            image = history.undo();
            pictureBox1.Image = image;
            pictureBox1.Refresh();
        }

        private void повторитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            image = history.redo();
            pictureBox1.Image = image;
            pictureBox1.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void инверсияToolStripMenuItem_Click(object sender, EventArgs e)
        {

            Filters filter = new FilterInvert();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void размытиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterBlur();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void фильтрГауссаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterGaussianFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void чернобелыйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterGrayScale();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void сепияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterClayScale();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void увеличениеЯркостиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterBrightness();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void увеличениеРезкостиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterRezkost();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void фильтрСобеляToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MatrixFilter2dSobelFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void тиснениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterTisnenie();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void motionBlurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterMotionBlurFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void операторЩарраToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MatrixFilter2dScharrFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void операторПрюиттаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MatrixFilter2dPrewittFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void операторСобеляToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MatrixFilter2dSobelFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void увеличениеРезкости2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterRezkost2();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void переносВлевоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterTransferFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void поворотToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterRotateFilter();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void волны1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterWaves1();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void волны2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterWaves2();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void медианныйФильтрToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterMedianFilter(3);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void фильтрмаксимумаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterMaximum();
            backgroundWorker1.RunWorkerAsync(filter);
        }
        
        private void светящиесяКраяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterGlowingEdges();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void линейнаяКоррекцияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterLinearCorrection();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void серыйМирToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new GrayWorld();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void идеальныйОтражательToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new IdealReflector();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void расширениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MorphologyDilation("square", 1);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void сужениеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MorphologyErosion("square", 1);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void открытиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MorphologyOpening("square", 1);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void закрытиеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MorphologyClosing("square", 1);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void морфологическийГрадиентToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MorphologyGradient("rhombus", 1);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void topHatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MorphologyTopHat("square", 4);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void blackHatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new MorphologyBlackHat("square", 4);
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void коррекцияСОпорнымЦветомToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pictureBox1.Image != null)
                permitClickOnPictureBox1 = true;
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (permitClickOnPictureBox1)
            {
                permitClickOnPictureBox1 = false;
                if (pictureBox1.Image != null)
                {
                    int pbHeight = pictureBox1.ClientSize.Height;
                    int pbWidth = pictureBox1.ClientSize.Width;
                    int imageHeight = image.Height;
                    int imageWidth = image.Width;
                    double wfactor = (double)imageWidth / pbWidth;
                    double hfactor = (double)imageHeight / pbHeight;
                    double resizeFactor = Math.Max(wfactor, hfactor);
                    int newImageSizeW = (int)(imageWidth / resizeFactor);
                    int newImageSizeH = (int)(imageHeight / resizeFactor);
                    int tabH = (pbHeight - newImageSizeH) / 2;
                    int tabW = (pbWidth - newImageSizeW) / 2;
                    int x = e.X - tabW;
                    int y = e.Y - tabH;
                    if (x >= 0 && y >= 0 && x < imageWidth && y < imageHeight)
                    {
                        int x0 = (x * imageWidth) / newImageSizeW;
                        int y0 = (y * imageHeight) / newImageSizeH;
                        Color sourceColor = image.GetPixel(x0, y0);

                        ColorDialog dialog = new ColorDialog();
                        dialog.FullOpen = true;
                        dialog.Color = sourceColor;
                        DialogResult result = dialog.ShowDialog();
                        if (result == DialogResult.OK)
                        {
                            Color resultColor = dialog.Color;
                            Filters filter = new BearingColor(x, y, sourceColor, resultColor);
                            backgroundWorker1.RunWorkerAsync(filter);
                        }
                    }
                }
                
            }
        }

        private void красноеСтеклоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterRedGlass();
            backgroundWorker1.RunWorkerAsync(filter);
        }

        private void стеклоToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Filters filter = new FilterGlassEffect();
            backgroundWorker1.RunWorkerAsync(filter);
        }

    }
}
