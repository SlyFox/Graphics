﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace lab1
{
    class FilterClayScale : FilterGrayScale
    {
        protected int k = 20;

        public FilterClayScale() { }

        public FilterClayScale(int k)
        {
            this.k = k;
        }

        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            Color intensity = base.calculateNewPixelColor(sourceImage, x, y);

            return Color.FromArgb(Clamp((int)(intensity.R + 2 * k), 0, 255),
                                  Clamp((int)(intensity.G + 0.5 * k), 0, 255),
                                  Clamp((int)(intensity.B - 1 * k), 0, 255)
                                  );
        }
    }
}
