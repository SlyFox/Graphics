﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;

namespace lab1
{
    class MorphologyOpening : Morphology
    {
        public MorphologyOpening(int[,] kernel, int num = 1)
        {
            this.kernel = kernel;
            this.num = num + num;
        }

        public MorphologyOpening(string s, int n, int num = 1)
        {
            this.kernel = chooseKernel(s, n);
            this.num = num + num;
        }

        protected override bool comparison(Color sourceColor, double best)
        {
            throw new NotImplementedException();
        }

        public override Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            Filters filter1 = new MorphologyErosion(kernel, num);
            Bitmap image1 = filter1.processImage(sourceImage, worker);
            Filters filter2 = new MorphologyDilation(kernel, num);
            Bitmap image2 = filter2.processImage(image1, worker);
            return image2;
        }
    }
}
