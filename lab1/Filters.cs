﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace lab1
{
    abstract class Filters
    {
        public Func<int, int, int, int> Clamp = (value, min, max) => Math.Min(Math.Max(min, value), max);

        public static Bitmap Clone(Bitmap source)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (Bitmap)formatter.Deserialize(stream);
            }
        }

        protected int num = 1;
        protected static int b = 0;

        public Filters()
        {
            num = 1;
        }

        public void setBtoZero()
        {
            b = 0;
        }

        protected abstract Color calculateNewPixelColor(Bitmap sourceImage, int x, int y);
        
        public virtual Bitmap processImage(Bitmap sourceImage, BackgroundWorker worker)
        {
            if (sourceImage != null) {
                int a = 100 / num;
                Bitmap resultImage = new Bitmap(sourceImage.Width, sourceImage.Height);
                for (int i = 0; i < sourceImage.Width; i++)
                {
                    worker.ReportProgress((int)((float)i / resultImage.Width * a + b));
                    if (worker.CancellationPending)
                        return null;
                    for (int j = 0; j < sourceImage.Height; j++)
                    {
                        resultImage.SetPixel(i, j, calculateNewPixelColor(sourceImage, i, j));
                    }
                }
                b += a;
                worker.ReportProgress(b);
                return resultImage;
            }
            return null;
        }
    }
}
